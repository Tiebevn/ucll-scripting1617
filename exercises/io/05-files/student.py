def files(path):
    """
    Geeft een lijst van bestandsnamen terug in de gegeven directory.
    Enkel bestanden moeten opgelijst worden, geen directories.

    Zoek zelf op hoe je in Python een lijst van bestanden opvraagt
    en hoe je kan weten of het gaat om een bestand of een directory.
    """

    raise NotImplementedError()


# Indien er een command line argument werd meegeleverd,
# stelt dit het pad voor waarin naar bestanden moeten worden
# gezocht.
# Indien er geen command line argument gegeven werd,
# wordt er gekeken in de huidige directory.
# Je kan verwijzen naar de huidige directory met '.'.
if __name__ == '__main__':
    # pass
