# Voorbeeld
def abs(x):
    if x < 0:
        return -x
    else:
        return x

# Merk op dat 'else if' in Python een speciale syntax heeft
# Zoek deze zelf op online
def sign(x):
    if x < 0:
        return -1
    elif x == 0:
        return 0
    else:
        return 1


# factorial
def factorial(x):
    if x < 0:
        return -factorial(-x)
    elif x == 0:
        return 1
    else:
        return x * factorial(x -1)
