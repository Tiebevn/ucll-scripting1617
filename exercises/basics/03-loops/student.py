def is_prime(n):
    for i in range(2, n):
        if n % i == 0:
            return False

    return n > 1


def count_primes_below(n):
    count = 0

    for i in range(0, n):
        if is_prime(i):
            count += 1

    return count


def gcd(x, y):

    x = abs(x)
    y = abs(y)

    for i in range(max(x, y), 0, -1):
        if x % i == 0 and y % i == 0:
            return i

    return 0


# Rekent het n-de getal van Fibonacci uit
def fibonacci(n):
    x = 0
    y = 1

    while n!= 0:
        x, y = y, x + y
        n -= 1
    return x

# Rekent de som van de cijfers van n op
def sum_digits(n):
    result = 0

    n = abs(n)
    while n > 0:
        result += n % 10
        n = n // 10

    return result


# Keer de cijfers van n om. Bv. 123 -> 321
def reverse_digits(n):

    result = 0

    if n < 0:
        return -reverse_digits(-n)
    else:
        while n > 0:
            result = result * 10 + (n % 10)
            n = n // 10

        return result


