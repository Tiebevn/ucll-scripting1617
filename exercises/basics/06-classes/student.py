class Counter:


    # Constructor
    def __init__(self):
        self.x = 0

    def current(self):
        return self.x

    def increment(self):
        self.x += 1

    def reset(self):
        self.x = 0
        
