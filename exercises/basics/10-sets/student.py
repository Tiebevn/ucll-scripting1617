def create_interval(a, b):
    result = set()

    for i in range(a, b + 1):
        result.add(i)

    return result

'''
     Kortere implementatie
    def create_interval(a, b):
       return set(range(a, b+1))
'''


def remove_duplicates_preserving_order(strings):
    result = []
    found = set()

    for string in strings:
        if string not in found:
            result.append(string)
            found.add(string)
    return result

# remove_duplicates_not_preserving_order


def remove_duplicates_not_preserving_order(strings):

    return list(set(strings))
# count_unique


def count_unique(strings):
    return len(set(strings))
