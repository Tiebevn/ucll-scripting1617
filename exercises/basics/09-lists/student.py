# Importeert de math module (nodig voor math.inf)
import math


def sum(ns):
    total = 0

    for n in ns:
        total += n

    return total


def interval(a, b):
    result = []
    for i in range(a, b+1):
        result.append(i)
    return result


def maximum(ns):

    result = -math.inf

    for n in ns:
        result = max(result, n)

    return result


def factors(n):

    result = []
    n = abs(n)
    k = 2

    while n > 1:
        if n % k == 0:
            result.append(k)
            n /= k
        else:
            k += 1
    return result


def remove_short_strings(strings, minimum_length):

    for i in range(len(strings), 0, -1):
        j = i-1
        if len(strings[j]) < minimum_length:
            del strings[j]


def longest_increasing_subsequence(xs):

    if len(xs) < 2:
        return xs

    counter = []
    tempcounter = []
    last = -math.inf

    for x in xs:
        if x >= last:
            tempcounter.append(x)
        else:
            tempcounter = [x]

        if len(counter) < len(tempcounter):
            counter = tempcounter
        last = x
    return counter




def largest_difference(ns):

    diff = -math.inf

    for i in ns:
        for j in ns:
            diff = max(diff, abs(i - j))
    return diff


def group(xs, n_groups):
    result = [[] for _ in range(0, n_groups)]

    for i in range(0, len(xs)):
        result[i % n_groups].append(xs[i])

    return result
