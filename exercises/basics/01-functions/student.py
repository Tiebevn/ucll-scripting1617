# Voorbeeld
def increment(x):
    return x + 1


def square(x):
    # Vervang onderstaande lijn door de vertaling van de Java-code
    return x * x


def are_ordered(x, y, z):
    return (x <= y & y <= z) or (x >= y & y >= z)


# is_divisible_by
def is_divisible_by(x, y):
    return y != 0 and x % y == 0
